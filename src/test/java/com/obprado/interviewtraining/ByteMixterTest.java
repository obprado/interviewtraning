/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.interviewtraining;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author omar
 * 
 * Problem extracted from "Cracking the Coding Interview, 4 Edition"
 * 
 * Chapter 5: Bit Manipulation
 * 
 * Page 58.
 * 
 * Problem 5.1
 * You are given two 32-bit numbers, N and M, and two bit positions, i and j. 
 * Write a method to set all bits between i and j in N equal to M 
 * (e.g., M becomes a substring of N located at i and starting at j).
            EXAMPLE:
            Input: N = 10000000000, M = 1101, i = 2, j = 6
            Output: N = 10000110100
 */
public class ByteMixterTest {
    
    private int N;
    private int M;
    private int i;
    private int j;    
    
    @Test
    public void testMixter(){
        givenN(0x0800).andM(0x000D).and_i(2).and_j(6);
        whenBytesAreMixed();
        N_ShouldBe(0x0834);
    }    
    
    private void whenBytesAreMixed(){
        N = ByteMixer.mix(N,M,i,j);
    }
    
    private void N_ShouldBe(int expected){
        assertEquals("The ByteMixer should have inyected M into N", expected, N);
    }
    
    private ByteMixterTest givenN(int N){
        this.N = N;
        return this;        
    }
    
    private ByteMixterTest andM(int M){
        this.M = M;
        return this;
    }
    
    private ByteMixterTest and_i(int i){
        this.i = i;
        return this;
    }
    
    private ByteMixterTest and_j(int j){
        this.j = j;
        return this;
    }
}
