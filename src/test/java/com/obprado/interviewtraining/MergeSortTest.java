/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.interviewtraining;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 * Test for the merge sort algorithm.
 */
public class MergeSortTest {
    
    private int[] unordered;
    private int[] ordered;    
    
    @Test
    public void emptyArrayShouldReturnedEmptyArray(){
        given(new int[0]);
        whenItsOrdered();
        thenTheArrayShouldBe(new int[0]);
    }
    
    @Test
    public void oneElementArrayShouldBeReturnedBack(){
        given(new int[] {5});
        whenItsOrdered();
        thenTheArrayShouldBe(new int[] {5});
    }
    
    @Test
    public void orderEvenSizedArray(){
        given(new int[] {5, 6, 500, 23, 2, 0});
        whenItsOrdered();
        thenTheArrayShouldBe(new int[] {0, 2, 5, 6, 23, 500});        
    }
    
     @Test
    public void orderOddSizedArray(){
        given(new int[] {5, 6, 10, 500, 23, 2, 0});
        whenItsOrdered();
        thenTheArrayShouldBe(new int[] {0, 2, 5, 6, 10, 23, 500});
    }
    
    @Test
    public void orderWithNegatives(){
        given(new int[] {3, -10, 7, 3289, -4783, 0, 2});
        whenItsOrdered();
        thenTheArrayShouldBe(new int[] {-4783, -10, 0, 2, 3, 7, 3289});
    }
    
    @Test
    public void theValuesShouldBeWithinTheIntegerRange(){
        given(integersOutOfBounds());
        whenItsOrdered();
        thenTheArrayShouldNotBe(orderderIntegersOutOfBounds());
    }        
    
    private void given(int[] input){
        this.unordered = input;
    }
    
    private void whenItsOrdered(){
        this.ordered = MergeSort.sort(this.unordered);
    }
    
    private void thenTheArrayShouldBe(int[] expected){
        assertArrayEquals("MergeSort failed to order the array", expected, ordered);
    }
    
    private void thenTheArrayShouldNotBe(int[] expected){
        boolean thereIsADifferentElement = false;
        for (int i = 0; i < this.ordered.length; i++){
            thereIsADifferentElement = thereIsADifferentElement || (expected[i] != this.ordered[i]);
        }
        assertTrue("MergeSort should have failed to order the array, it didn't", thereIsADifferentElement);
    }

    private int[] integersOutOfBounds(){
        int over_max = Integer.MAX_VALUE + 3;
        int under_min = Integer.MIN_VALUE - 3;
        return new int[] {2, over_max, -3, 0, under_min, 7548};
    }
    
    private int[] orderderIntegersOutOfBounds(){
        int over_max = Integer.MAX_VALUE + 3;
        int under_min = Integer.MIN_VALUE - 3;
        return new int[] {under_min, -3, 0, 2,  7548, over_max};
    }
    
}
