/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.interviewtraining;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author omar
 */
public class JavaBytePrimitivesTest {
    
    @Test
    public void testByte(){
        Byte F =   Byte.valueOf("00000011");
        Byte two = Byte.valueOf("00000001");
        assertEquals("The Byte.valueOf method doesn't work as expected", "11", Byte.toString(F));
        assertEquals("The Byte.valueOf method doesn't work as expected", "1", Byte.toString(two));
    }
    
    @Test
    public void testBitInverterOnByte(){        
        int expected = 0xFFFFFFFF;
        int actual = ~0x00000000;
        
        assertEquals("The bitwise complement operator doesn't work as expected",
                expected, actual);
    }
    
}
