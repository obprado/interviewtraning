/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.interviewtraining;

/**
 *
 * @author omar
 * Merge sort algorithm.
 * //TODO: refactor it.
 */
public class MergeSort {
    
    public static int[] sort(int[] array){
        
        //Check for the base case
        if (array.length <= 1){
            return array;
        }
        
        //split array in two smaller arrays        
        //find the splitting point
        int leftArrayLength, rightArrayStartingPoint;
        int[] leftArray, rightArray;
        leftArrayLength = array.length / 2;
        
        //do the actual splitting
        leftArray = new int[leftArrayLength];
        rightArray = new int[array.length - leftArrayLength];
        int copied = 0;
        for ( ; copied < leftArray.length; copied++){
            leftArray[copied] = array[copied];
        }        
        for ( ; copied < array.length; copied++){
            rightArray[copied - leftArray.length] = array[copied];
        }
        
        //recurse
        int [] leftOrdered = MergeSort.sort(leftArray);
        int [] rightOrdered = MergeSort.sort(rightArray);
        
        //order and merge
        int[] merged = new int[array.length];
        boolean oneArrayIsConsumed = false;
        int i = 0;
        int j = 0;
        int k = 0;
        while(!oneArrayIsConsumed){
            if (leftOrdered[i] < rightOrdered[j]){
                merged[k] = leftOrdered[i];
                oneArrayIsConsumed = oneArrayIsConsumed || i == (leftOrdered.length - 1);
                i++;                
            } else {
                merged[k] = rightOrdered[j];                
                oneArrayIsConsumed = oneArrayIsConsumed || j == (rightOrdered.length - 1);                
                j++;
            }
            k++;
        }
        for (    ; i < leftOrdered.length ; i++ ){
            merged[k] = leftOrdered[i];
            k++;
        }
        for (    ; j < rightOrdered.length ; j++ ){
            merged[k] = rightOrdered[j];
            k++;
        }
        return merged;
    }
    
}
