/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.interviewtraining;

/**
 *
 * @author omar
 * 
 * Problem extracted from "Cracking the Coding Interview, 4 Edition"
 * 
 * Chapter 5: Bit Manipulation
 * 
 * Page 58.
 * 
 * Problem 5.1
 * You are given two 32-bit numbers, N and M, and two bit positions, i and j. 
 * Write a method to set all bits between i and j in N equal to M 
 * (e.g., M becomes a substring of N located at i and starting at j).
            EXAMPLE:
            Input: N = 10000000000, M = 1101, i = 2, j = 6
            Output: N = 10000110100
 */
public class ByteMixer {
    
    public static int mix(int N, int M, int i, int j){
        int bitMask = createMask(i,j);
        //Wipes the values in N according to i and j
        N = N & bitMask;
        //Moves the bits on M to the left until it fits on the wiped bits of N
        M = M << i;
        //Mixes M & N
        return M | N;
        
    }
    
    private static int createMask(int onesAtRight, int zerosAtRigth){
        int zerosAtLeft = 32 - onesAtRight;
        int onesAtLeft = 0xFFFFFFFF << zerosAtRigth;
        int onesAtRigth = 0xFFFFFFFF >>> zerosAtLeft;
        return onesAtLeft | onesAtRigth;
    }
    
}
